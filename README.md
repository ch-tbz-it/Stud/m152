# M152 - Multimedia-Inhalte in Webauftritt integrieren

[> **Modulidentifikation** ](https://cf.ict-berufsbildung.ch/modules.php?name=Mbk&a=20101&cmodnr=152&noheader=1)

[> **Bewertungsraster** ](https://docs.google.com/spreadsheets/d/10biUbNsA-Uf0LPIsZng3Tjh104b-ZDa7TDFd2m1Ph3Q/edit#gid=0 )

- [Ressourcen](./auftraege/x_gitressourcen)
- [Buecher_Skripts_Praesentationen](./auftraege/x_gitressourcen/Buecher_Skripts_Praesentationen)
- [Buecher_Skripts_Praesentationen/Skript](./auftraege/x_gitressourcen/Buecher_Skripts_Praesentationen/Modul_152_Script_3.pdf)

## Aufträge & Übungen
| Tag  |  Komp. | Nr.    | Auftrag, Übung, Thema |
| ---- | ------ | ------ | -------------- |
|   1  |  K1    | M152-a | [Webspace buchen und erste Website erstellen](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/a) |
|   2  |  K1    | M152-b | [Storytelling, Wireframes, Mockups](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/b) |
|   3  |  K1    | M152-c | [Eigener Styleguide definieren/bestimmen und dokumentieren](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/c) |
|   4  |  K2    | M152-d | [Urheberrecht](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/d) |
|   4  |  K2    | M152-e | [Copyrights und Lizenzen im Internet](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/e) |
|   5  |  K2    | M152-f | [DSGVO, Cookie-Warnung, Impressum](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/f) |
|   6  |  K3    | M152-g | [Bildformate, Videoformate, Soundformate erstellen und vergleichen](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/g) |
|   7  |  K4    | M152-h | [Animationen erstellen (anim-CSS, anim-GIF, SVG, Keyframes, Transitions, Canvas)](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/h) |
|   8  |  K5, K6 | M152-i | [Testen / Automatisierung & autom. Formulare ausfüllen](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/i) |
|   9  |  K6    | M152-j | [Optimierung, Page speed](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/j) |
